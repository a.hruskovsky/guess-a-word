pub const UNIX_SOCK_PATH: &str = "/tmp/guess_a_word_sock";
pub const BUFFER_LEN: usize = 1024; // index where payload data starts
pub const MAX_USER_INPUT_LEN: usize = 512; // Just value lower than BUFFER_LEN

#[derive(PartialEq)]
pub enum CommunicationType {
    UnixSocket,
    Tcp,
}

#[derive(Debug, Clone, PartialEq)]
#[repr(u8)]
pub enum MessageType {
    Initiate = 1,
    Disconnect,
    Authenticate,
    AuthenticateResponse,
    ListClients,
    ListClientsResponse,
    GameRequest,
    GameRequestResponse,
    GameAction,
    GameState,
    ErrorResponse,
}

impl MessageType {
    fn from_u8(value: u8) -> Option<Self> {
        match value {
            1 => Some(MessageType::Initiate),
            2 => Some(MessageType::Disconnect),
            3 => Some(MessageType::Authenticate),
            4 => Some(MessageType::AuthenticateResponse),
            5 => Some(MessageType::ListClients),
            6 => Some(MessageType::ListClientsResponse),
            7 => Some(MessageType::GameRequest),
            8 => Some(MessageType::GameRequestResponse),
            9 => Some(MessageType::GameAction),
            10 => Some(MessageType::GameState),
            11 => Some(MessageType::ErrorResponse),
            _ => None,
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
#[repr(u8)]
pub enum ErrorCode {
    NoError = 0,
    OpponentNotExist,
    OpponentAlreadyInGame,
    OpponentIsYou,
    FormatError,
}

impl ErrorCode {
    fn from_u8(value: u8) -> Option<Self> {
        match value {
            0 => Some(ErrorCode::NoError),
            1 => Some(ErrorCode::OpponentNotExist),
            2 => Some(ErrorCode::OpponentAlreadyInGame),
            3 => Some(ErrorCode::OpponentIsYou),
            4 => Some(ErrorCode::FormatError),
            _ => None,
        }
    }
}

#[derive(Debug)]
pub struct MessageHeader {
    pub message_type: MessageType,
    pub length: u16,
}

#[derive(Debug)]
pub struct Message {
    pub header: MessageHeader,
    pub payload: Vec<u8>, // Binary payload
}

impl Message {
    pub fn new(message_type: MessageType, payload: &[u8]) -> Message {
        Message {
            header: MessageHeader {
                message_type,
                length: payload.len() as u16,
            },
            payload: payload.to_vec(),
        }
    }
}

#[derive(Debug)]
pub struct ErrorPayload {
    pub code: ErrorCode,
    pub text: String,
}

impl ErrorPayload {
    pub fn serialize(&self) -> Vec<u8> {
        let code_byte = self.code.clone() as u8;
        let text_bytes = self.text.as_bytes();
        let text_length = (text_bytes.len() as u32).to_be_bytes();

        let mut buffer = Vec::new();
        buffer.push(code_byte);
        buffer.extend_from_slice(&text_length);
        buffer.extend_from_slice(text_bytes);

        buffer
    }

    pub fn deserialize(buffer: &[u8]) -> Option<ErrorPayload> {
        if buffer.len() < 5 {
            return None;
        }

        let code = match ErrorCode::from_u8(buffer[0]) {
            Some(code) => code,
            None => return None,
        };

        let text_length = u32::from_be_bytes([buffer[1], buffer[2], buffer[3], buffer[4]]) as usize;

        if buffer.len() < 5 + text_length {
            return None;
        }

        let text = match std::str::from_utf8(&buffer[5..5 + text_length]) {
            Ok(text) => text.to_string(),
            Err(_) => return None,
        };

        Some(ErrorPayload { code, text })
    }
}

#[derive(Debug)]
pub struct AuthPayload {
    pub password: String,
}

#[derive(Debug)]
pub struct AuthResponsePayload {
    pub status: bool,
    pub client_id: Option<u32>,
}

impl AuthResponsePayload {
    pub fn serialize(&self) -> Vec<u8> {
        let mut bytes = Vec::new();
        bytes.push(self.status as u8);

        match self.client_id {
            Some(id) => {
                bytes.push(1); // Presence indicator
                bytes.extend_from_slice(&id.to_be_bytes()); // Append u32 in big-endian
            }
            None => {
                bytes.push(0); // Absence indicator
            }
        }

        bytes
    }

    pub fn deserialize(bytes: &[u8]) -> Option<AuthResponsePayload> {
        if bytes.len() < 2 {
            return None; // Not enough data
        }

        let status = bytes[0] != 0;
        let has_id = bytes[1] != 0;

        let client_id = if has_id {
            if bytes.len() >= 6 {
                let mut array = [0u8; 4];
                array.copy_from_slice(&bytes[2..6]);
                Some(u32::from_be_bytes(array))
            } else {
                return None; // Not enough bytes for u32
            }
        } else {
            None
        };

        Some(AuthResponsePayload { status, client_id })
    }
}

#[derive(Debug)]
pub struct ClientsListPayload {
    pub client_ids: Vec<u32>,
}

impl ClientsListPayload {
    pub fn serialize(&self) -> Vec<u8> {
        let mut bytes = Vec::with_capacity(self.client_ids.len() * 4);
        for &id in &self.client_ids {
            bytes.extend_from_slice(&id.to_le_bytes());
        }
        bytes
    }

    pub fn deserialize(data: &[u8]) -> Option<ClientsListPayload> {
        if data.len() % 4 != 0 {
            return None; // Data length must be a multiple of 4
        }

        let mut client_ids = Vec::with_capacity(data.len() / 4);
        for chunk in data.chunks(4) {
            let id = u32::from_le_bytes(chunk.try_into().expect("Slice with incorrect size"));
            client_ids.push(id);
        }

        Some(ClientsListPayload { client_ids })
    }
}

#[derive(Debug)]
pub struct GameRequestPayload {
    pub opponent_id: u32,
    pub word_to_guess: String,
}

impl GameRequestPayload {
    pub fn serialize(&self) -> Vec<u8> {
        let mut bytes = Vec::new();
        bytes.extend(&self.opponent_id.to_le_bytes());

        let word_bytes = self.word_to_guess.as_bytes();
        let word_length = word_bytes.len() as u32;
        bytes.extend(&word_length.to_le_bytes()); // Prepend length as u32
        bytes.extend(word_bytes); // Add the actual string bytes

        bytes
    }

    pub fn deserialize(data: &[u8]) -> Option<GameRequestPayload> {
        if data.len() < 8 {
            return None; // Not enough data to contain both an ID and a length
        }

        let (opponent_id_bytes, rest) = data.split_at(4);
        let opponent_id = u32::from_le_bytes(opponent_id_bytes.try_into().ok()?);

        let (length_bytes, string_bytes) = rest.split_at(4);
        let string_length = u32::from_le_bytes(length_bytes.try_into().ok()?);

        if string_bytes.len() < string_length as usize {
            return None; // The byte slice does not contain enough bytes as specified by `string_length`
        }

        let word_to_guess = match String::from_utf8(string_bytes[..string_length as usize].to_vec())
        {
            Ok(s) => s,
            Err(_) => return None,
        };

        Some(GameRequestPayload {
            opponent_id,
            word_to_guess,
        })
    }
}

#[derive(Debug, Clone, PartialEq)]
#[repr(u8)]
pub enum GameActionType {
    GuessWord = 1,
    Guess,
    Hint,
    Surrender,
    Win,
}

impl GameActionType {
    fn from_u8(value: u8) -> Option<Self> {
        match value {
            1 => Some(GameActionType::GuessWord),
            2 => Some(GameActionType::Guess),
            3 => Some(GameActionType::Hint),
            4 => Some(GameActionType::Surrender),
            5 => Some(GameActionType::Win),
            _ => None,
        }
    }
}

#[derive(Debug)]
pub struct GameActionPayload {
    pub action_type: GameActionType,
    pub content: String,
}

impl GameActionPayload {
    pub fn serialize(&self) -> Vec<u8> {
        let action_type_byte = self.action_type.clone() as u8;
        let content_bytes = self.content.as_bytes();
        let content_length = (content_bytes.len() as u32).to_be_bytes();

        let mut buffer = Vec::new();
        buffer.push(action_type_byte);
        buffer.extend_from_slice(&content_length);
        buffer.extend_from_slice(content_bytes);

        buffer
    }

    pub fn deserialize(buffer: &[u8]) -> Option<GameActionPayload> {
        if buffer.len() < 5 {
            return None;
        }

        let action_type = match GameActionType::from_u8(buffer[0]) {
            Some(action_type) => action_type,
            None => return None,
        };

        let content_length =
            u32::from_be_bytes([buffer[1], buffer[2], buffer[3], buffer[4]]) as usize;

        if buffer.len() < 5 + content_length {
            return None;
        }

        let content = match std::str::from_utf8(&buffer[5..5 + content_length]) {
            Ok(content) => content.to_string(),
            Err(_) => return None,
        };

        Some(GameActionPayload {
            action_type,
            content,
        })
    }
}

pub struct GameStatePayload {
    pub current_guesses: u32,
    pub hint: String,
}

impl GameStatePayload {
    pub fn serialize(&self) -> Vec<u8> {
        let mut bytes = Vec::new();
        bytes.extend(&self.current_guesses.to_le_bytes());

        let hint_bytes = self.hint.as_bytes();
        let hint_length = hint_bytes.len() as u32;
        bytes.extend(&hint_length.to_le_bytes()); // Prepend length as u32
        bytes.extend(hint_bytes); // Add the actual string bytes

        bytes
    }

    pub fn deserialize(data: &[u8]) -> Option<GameStatePayload> {
        if data.len() < 8 {
            return None; // Not enough data to contain both an ID and a length
        }

        let (current_guesses_bytes, rest) = data.split_at(4);
        let current_guesses = u32::from_le_bytes(current_guesses_bytes.try_into().ok()?);

        let (length_bytes, string_bytes) = rest.split_at(4);
        let string_length = u32::from_le_bytes(length_bytes.try_into().ok()?);

        if string_bytes.len() < string_length as usize {
            return None; // The byte slice does not contain enough bytes as specified by `string_length`
        }

        let hint = match String::from_utf8(string_bytes[..string_length as usize].to_vec()) {
            Ok(s) => s,
            Err(_) => return None,
        };

        Some(GameStatePayload {
            current_guesses,
            hint,
        })
    }
}

use std::{
    hash::{DefaultHasher, Hash, Hasher},
    io::{self, Cursor, Error, Read},
};

pub fn serialize_message(msg: &Message) -> Vec<u8> {
    let mut bytes = Vec::with_capacity(BUFFER_LEN);
    bytes.push(msg.header.message_type.clone() as u8);
    bytes.extend_from_slice(&msg.header.length.to_le_bytes());
    bytes.extend(&msg.payload);
    bytes
}

pub fn deserialize_message(input: &[u8]) -> io::Result<Message> {
    let mut cursor = Cursor::new(input);
    let mut message_type = [0u8; 1];
    let mut length_bytes = [0u8; 2];

    cursor.read_exact(&mut message_type)?;
    cursor.read_exact(&mut length_bytes)?;

    let length = u16::from_le_bytes(length_bytes);
    let mut payload = vec![0u8; length as usize];
    cursor.read_exact(&mut payload)?;

    match MessageType::from_u8(message_type[0]) {
        Some(message_type) => Ok(Message {
            header: MessageHeader {
                message_type,
                length,
            },
            payload,
        }),
        None => Err(Error::last_os_error()),
    }
}

pub fn hash<T: Hash>(item: &T) -> u64 {
    let mut hasher = DefaultHasher::new();
    item.hash(&mut hasher);
    hasher.finish()
}
