pub trait NonBlocking {
    fn set_nonblocking(&self, nonblocking: bool) -> std::io::Result<()>;
}

use std::io;
use std::net::TcpStream;
use std::os::unix::net::UnixStream;

impl NonBlocking for TcpStream {
    fn set_nonblocking(&self, nonblocking: bool) -> io::Result<()> {
        TcpStream::set_nonblocking(self, nonblocking)
    }
}

impl NonBlocking for UnixStream {
    fn set_nonblocking(&self, nonblocking: bool) -> io::Result<()> {
        UnixStream::set_nonblocking(self, nonblocking)
    }
}
