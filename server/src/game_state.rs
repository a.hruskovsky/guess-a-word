#[derive(Debug, Clone)]
pub enum GameStatus {
    Ongoing,
    Completed,
    Surrendered,
}

#[derive(Debug, Clone)]
pub struct GameState {
    pub game_id: u32,
    pub player_ids: (u32, u32), // (Word Setter ID, Guesser ID)
    word_to_guess: String,
    pub current_guesses: u32,
    hints: Vec<String>,
    pub status: GameStatus,
}

impl GameState {
    // Constructs a new GameState with initial values
    pub fn new(game_id: u32, setter_id: u32, guesser_id: u32, word: String) -> GameState {
        GameState {
            game_id,
            player_ids: (setter_id, guesser_id),
            word_to_guess: word,
            current_guesses: 0,
            hints: Vec::new(),
            status: GameStatus::Ongoing,
        }
    }

    // Adds a hint to the game state
    pub fn add_hint(&mut self, hint: String) {
        self.hints.push(hint);
    }

    // Updates the number of guesses
    fn update_guesses(&mut self) {
        self.current_guesses += 1;
    }

    // return number of guesses
    pub fn get_guesses(&self) -> u32 {
        self.current_guesses
    }

    // Checks if the guess is correct
    pub fn check_guess(&mut self, guess: &str) -> bool {
        self.update_guesses();
        if self.word_to_guess == guess {
            self.status = GameStatus::Completed;
            true
        } else {
            false
        }
    }

    // Allows a player to surrender
    pub fn surrender(&mut self) {
        self.status = GameStatus::Surrendered;
    }

    pub fn win(&mut self) {
        self.status = GameStatus::Completed;
    }

    pub fn get_opponent_id(&self, player_id: u32) -> Option<u32> {
        let (setter_id, guesser_id) = self.player_ids;
        if player_id == setter_id {
            Some(guesser_id)
        } else if player_id == guesser_id {
            Some(setter_id)
        } else {
            None // The given player_id is not part of this game
        }
    }
}
