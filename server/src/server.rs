const SECRET_PASSWORD: &str = "password";

use common::{
    deserialize_message, hash, serialize_message, AuthResponsePayload, ClientsListPayload,
    ErrorPayload, GameActionPayload, GameActionType, GameRequestPayload, GameStatePayload, Message,
    MessageType, BUFFER_LEN, UNIX_SOCK_PATH,
};
use std::{
    collections::HashMap,
    fs,
    io::{self, Read, Write},
    net::{TcpListener, TcpStream},
    os::unix::net::UnixListener,
    sync::{
        mpsc::{self, Sender},
        Arc, Mutex, MutexGuard,
    },
    time::Duration,
    vec,
};

use crate::{
    game_state::{GameState, GameStatus},
    traits::NonBlocking,
};

pub struct ServerContext {
    clients: Mutex<HashMap<u32, Sender<Message>>>,
    games: Mutex<HashMap<u32, GameState>>,
}

impl ServerContext {
    pub fn new() -> ServerContext {
        ServerContext {
            clients: Mutex::new(HashMap::new()),
            games: Mutex::new(HashMap::new()),
        }
    }

    pub fn add_new_game(&self, game: GameState) {
        match self.games.lock() {
            Ok(mut lock) => {
                let _result = lock.insert(game.game_id, game);
            }
            Err(poisoned) => {
                // Here it depends if gracefully shut the server down or try to rely on underlying data
                // I've chosen second approach for this testing application
                eprintln!("Mutex is poisoned. Recovering...");
                poisoned.into_inner().insert(game.game_id, game);
            }
        }
    }

    fn get_all_clients(&self) -> Vec<u32> {
        match self.clients.lock() {
            Ok(lock) => {
                let mut clients = lock.keys().cloned().collect::<Vec<u32>>();
                clients.sort();
                clients
            }
            Err(poisoned) => {
                // Here it depends if gracefully shut the server down or try to rely on underlying data
                // I've chosen second approach for this testing application
                eprintln!("Mutex is poisoned. Recovering...");
                poisoned.into_inner().keys().cloned().collect()
            }
        }
    }

    pub fn get_clients_not_in_games(&self) -> Vec<u32> {
        let all_clients = self.get_all_clients();

        let player_ids_in_games: Vec<u32> = match self.games.lock() {
            Ok(games) => games
                .values()
                .filter(|game| matches!(game.status, GameStatus::Ongoing)) // Only consider Ongoing games
                .flat_map(|game| vec![game.player_ids.0, game.player_ids.1])
                .collect(),
            Err(poisoned) => {
                eprintln!("Mutex is poisoned. Recovering...");
                poisoned
                    .into_inner()
                    .values()
                    .filter(|game| matches!(game.status, GameStatus::Ongoing)) // Same filter for recovering
                    .flat_map(|game| vec![game.player_ids.0, game.player_ids.1])
                    .collect()
            }
        };

        let mut clients = all_clients
            .into_iter()
            .filter(|id| !player_ids_in_games.contains(id))
            .collect::<Vec<u32>>();
        clients.sort();
        clients
    }

    pub fn is_available_for_play(&self, client_id: u32) -> bool {
        let not_in_game = self.get_clients_not_in_games();
        not_in_game.contains(&client_id)
    }

    pub fn find_game_key_by_player_id(&self, player_id: u32) -> Option<u32> {
        let games = match self.games.lock() {
            Ok(lock) => lock,
            Err(poisoned) => {
                // Here it depends if gracefully shut the server down or try to rely on underlying data
                // I've chosen second approach for this testing application
                eprintln!("Mutex is poisoned. Recovering...");
                poisoned.into_inner() // Recover the data even though it's poisoned
            }
        };

        games.iter().find_map(|(key, game)| {
            if game.player_ids.0 == player_id || game.player_ids.1 == player_id {
                Some(*key)
            } else {
                None
            }
        })
    }
}

// Starts a new TCP listener in a separate thread
pub fn start_tcp_listener_thread(
    server: Arc<ServerContext>,
    port: u16,
) -> std::io::Result<std::thread::JoinHandle<()>> {
    let listener = TcpListener::bind(format!("127.0.0.1:{}", port))?;
    println!("Server is listening on 127.0.0.1:{}", port);
    Ok(std::thread::spawn(move || {
        listener_accept_loop(server, listener.incoming())
    }))
}

// Starts a new TCP listener in a separate thread
pub fn start_unix_listener_thread(
    server: Arc<ServerContext>,
) -> std::io::Result<std::thread::JoinHandle<()>> {
    cleanup_unix_socket();
    let listener = UnixListener::bind(UNIX_SOCK_PATH)?;
    println!("Server is listening on Unix socket at {}", UNIX_SOCK_PATH);
    Ok(std::thread::spawn(move || {
        listener_accept_loop(server, listener.incoming())
    }))
}

pub fn start_http_listener_thread(
    server: Arc<ServerContext>,
) -> std::io::Result<std::thread::JoinHandle<()>> {
    let listener = TcpListener::bind("127.0.0.1:8080")?;
    println!("Server is listening on 127.0.0.1:8080");
    Ok(std::thread::spawn(move || {
        for stream in listener.incoming() {
            match stream {
                Ok(stream) => {
                    handle_http_client(stream, server.clone());
                }
                Err(e) => eprintln!("Failed to establish a connection: {}", e),
            }
        }
    }))
}

// Accepts all incoming connection in a loop, spawning a new thread for every accepted connection, where the connection is handled.
fn listener_accept_loop<L, S>(server: Arc<ServerContext>, listener: L)
where
    L: IntoIterator<Item = std::io::Result<S>>,
    S: NonBlocking + Read + Write + Send + 'static,
{
    for stream in listener {
        match stream {
            Ok(stream) => {
                stream
                    .set_nonblocking(true)
                    .expect("Can not set stream as nonblocking");
                let server = server.clone();
                let client_id = get_id();
                std::thread::spawn(move || handle_client(client_id, server, stream));
            }
            Err(e) => eprintln!("Failed to accept connection: {}", e),
        }
    }
}

fn handle_http_client(mut stream: TcpStream, server: Arc<ServerContext>) {
    let mut buffer = [0; BUFFER_LEN];
    match stream.read(&mut buffer) {
        Ok(0) => {
            println!("Client disconnected");
            return;
        }
        Ok(_size) => {
            // Everything is okay, processing continue bellow
        }
        Err(e) => {
            eprintln!("Error: {}", e);
            return;
        }
    }

    let request = std::str::from_utf8(&buffer).unwrap();

    if request.starts_with("GET /games") {
        let games_lock = server.games.lock().unwrap();
        let games_data = games_lock
            .iter()
            .map(|(_, game)| {
                format!(
                    r#"{{"game_id":{},"players":"{:?}","attempts":{},"status":"{:?}"}}"#,
                    game.game_id, game.player_ids, game.current_guesses, game.status
                )
            })
            .collect::<Vec<String>>()
            .join(",");

        let response = format!(
            "HTTP/1.1 200 OK\r\nAccess-Control-Allow-Origin: *\r\nContent-Type: application/json\r\n\r\n{{\"games\":[{}]}}",
            games_data
        );
        stream.write_all(response.as_bytes()).unwrap();
    } else {
        let response = "HTTP/1.1 404 NOT FOUND\r\nAccess-Control-Allow-Origin: *\r\n\r\n";
        stream.write_all(response.as_bytes()).unwrap();
    }

    stream.flush().unwrap();
}

fn handle_client<S: Read + Write>(client_id: u32, server: Arc<ServerContext>, mut stream: S) {
    println!("Client connected");

    // Initiate new connection
    let initiate_msg = Message::new(MessageType::Initiate, "Hello client!".as_bytes());
    write_all_to_stream(&mut stream, &serialize_message(&initiate_msg));

    let (tx, rx) = mpsc::channel::<Message>();

    loop {
        // Check for messages from other clients
        if let Ok(msg) = rx.try_recv() {
            if let Err(e) = stream.write_all(&serialize_message(&msg)) {
                eprintln!("Failed to send message to client {}: {}", client_id, e);
                break;
            }
        }

        let mut buffer = [0u8; BUFFER_LEN];
        match stream.read(&mut buffer) {
            Ok(0) => {
                println!("Client disconnected");
                // remove client from the list
                match server.clients.lock() {
                    Ok(mut lock) => {
                        if let Some(_id) = lock.remove(&client_id) {
                            println!("Client ID {} removed from the server list", client_id);
                        }
                    }
                    Err(poisoned) => {
                        // Here it depends if gracefully shut the server down or try to rely on underlying data
                        // I've chosen second approach for this testing application
                        eprintln!("Mutex is poisoned. Recovering...");
                        if let Some(_id) = poisoned.into_inner().remove(&client_id) {
                            println!("Client ID {} removed from the server list", client_id);
                        }
                    }
                }
                break;
            }
            Ok(_size) => match deserialize_message(&buffer) {
                Ok(message) => {
                    if process_message(client_id, server.clone(), message, &mut stream, tx.clone())
                        .is_err()
                    {
                        break;
                    }
                }
                Err(_) => {
                    let error_payload = ErrorPayload {
                        code: common::ErrorCode::FormatError,
                        text: "Error message format".to_string(),
                    };
                    let error_msg =
                        Message::new(MessageType::ErrorResponse, &error_payload.serialize());
                    write_all_to_stream(&mut stream, &serialize_message(&error_msg))
                }
            },
            Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                // No data available yet
                continue; // Optionally sleep or yield
            }
            Err(e) => {
                println!("Error: {}", e);
                break;
            }
        }
        std::thread::sleep(Duration::from_millis(10));
    }
}

fn process_message<S: Read + Write>(
    client_id: u32,
    server: Arc<ServerContext>,
    message: Message,
    stream: &mut S,
    tx: Sender<Message>,
) -> Result<(), String> {
    match message.header.message_type {
        MessageType::Authenticate => {
            // Process authentication
            if message.payload == hash(&SECRET_PASSWORD).to_string().as_bytes().to_vec() {
                println!("Client was authenticated. ID {} was assigned", client_id);

                match server.clients.lock() {
                    Ok(mut lock) => {
                        lock.insert(client_id, tx);
                    }
                    Err(poisoned) => {
                        // Here it depends if gracefully shut the server down or try to rely on underlying data
                        // I've chosen second approach for this testing application
                        eprintln!("Mutex is poisoned. Recovering...");
                        poisoned.into_inner().insert(client_id, tx);
                    }
                }

                let auth_response_payload = AuthResponsePayload {
                    status: true,
                    client_id: Some(client_id),
                };
                let auth_msg = Message::new(
                    MessageType::AuthenticateResponse,
                    &auth_response_payload.serialize(),
                );
                write_all_to_stream(stream, &serialize_message(&auth_msg));

                return Ok(());
            } else {
                let auth_response_payload = AuthResponsePayload {
                    status: false,
                    client_id: None,
                };
                let auth_msg = Message::new(
                    MessageType::AuthenticateResponse,
                    &auth_response_payload.serialize(),
                );
                write_all_to_stream(stream, &serialize_message(&auth_msg));

                return Err("Authentication failed! Client will be disconnected".to_string());
            }
        }
        MessageType::ListClients => {
            let mut all_clients = server.get_clients_not_in_games();
            all_clients.retain(|&c| c != client_id);

            let clients_list_payload = ClientsListPayload {
                client_ids: all_clients,
            };
            let clients_list = Message::new(
                MessageType::ListClientsResponse,
                &clients_list_payload.serialize(),
            );
            write_all_to_stream(stream, &serialize_message(&clients_list));
        }
        MessageType::GameRequest => {
            match GameRequestPayload::deserialize(&message.payload) {
                Some(p) => {
                    let is_opponent_available = server.is_available_for_play(p.opponent_id);
                    let c_lock = lock_client_mutex(&server.clients);

                    match c_lock.get(&p.opponent_id) {
                        Some(opponent) => {
                            // Check if player don't try to set himself
                            if p.opponent_id == client_id {
                                let payload = ErrorPayload {
                                    code: common::ErrorCode::OpponentIsYou,
                                    text: "You can not play against yourself".to_string(),
                                };
                                let game_req_resp = Message::new(
                                    MessageType::GameRequestResponse,
                                    &payload.serialize(),
                                );
                                write_all_to_stream(stream, &serialize_message(&game_req_resp));

                                return Ok(());
                            }

                            // Check if opponent is available for game
                            if !is_opponent_available {
                                // Opponent not available - Response to client
                                let payload = ErrorPayload {
                                    code: common::ErrorCode::OpponentAlreadyInGame,
                                    text:
                                        "Opponent is already in different game. Choose another one."
                                            .to_string(),
                                };
                                let game_req_resp = Message::new(
                                    MessageType::GameRequestResponse,
                                    &payload.serialize(),
                                );
                                write_all_to_stream(stream, &serialize_message(&game_req_resp));

                                return Ok(());
                            }

                            let ga_payload = GameActionPayload {
                                action_type: common::GameActionType::GuessWord,
                                content: format!(
                                    "Player id {} want you to guess his word. When your guess will be right, you will be notified",
                                    client_id
                                ),
                            };
                            let message =
                                Message::new(MessageType::GameAction, &ga_payload.serialize());

                            opponent.send(message).map_err(|send_error| {
                                format!(
                                    "Failed to send message to opponent id {}: {:?}",
                                    p.opponent_id, send_error
                                )
                            })?;

                            // Create new GameState
                            let new_game = GameState::new(
                                get_game_id(),
                                client_id,
                                p.opponent_id,
                                p.word_to_guess,
                            );
                            server.add_new_game(new_game);

                            // Response to client
                            let payload = ErrorPayload {
                                code: common::ErrorCode::NoError,
                                text: "".to_string(),
                            };
                            let game_req_resp = Message::new(
                                MessageType::GameRequestResponse,
                                &payload.serialize(),
                            );
                            write_all_to_stream(stream, &serialize_message(&game_req_resp));
                        }
                        None => {
                            let error_text =
                                format!("Opponent ID {} does not exist", p.opponent_id);
                            let error_payload = ErrorPayload {
                                code: common::ErrorCode::OpponentNotExist,
                                text: error_text,
                            };
                            let error_msg = Message::new(
                                MessageType::GameRequestResponse,
                                &error_payload.serialize(),
                            );
                            write_all_to_stream(stream, &serialize_message(&error_msg));
                        }
                    }
                }
                None => {
                    let error_payload = ErrorPayload {
                        code: common::ErrorCode::FormatError,
                        text: "Error message format".to_string(),
                    };
                    let error_msg =
                        Message::new(MessageType::ErrorResponse, &error_payload.serialize());
                    write_all_to_stream(stream, &serialize_message(&error_msg))
                }
            }
        }
        MessageType::GameAction => {
            let game_id = server.find_game_key_by_player_id(client_id);
            let game_action_payload = match GameActionPayload::deserialize(&message.payload) {
                Some(payload) => payload,
                None => {
                    return Err("MessageType::GameAction - Payload is corrupted".to_string());
                }
            };

            if let Some(game_id) = game_id {
                match game_action_payload.action_type {
                    GameActionType::Guess => {
                        let mut g_lock = lock_game_mutex(&server.games);

                        if let Some(game) = g_lock.get_mut(&game_id) {
                            // Get opponent_id and stream
                            if game.get_opponent_id(client_id).is_none() {
                                return Err("Unexpected error, player has no opponent".to_string());
                            }
                            let opponent_id = game
                                .get_opponent_id(client_id)
                                .expect("This error should not happened");

                            // Check if Guesser find the right word
                            if game.check_guess(&game_action_payload.content) {
                                let game_action_payload = GameActionPayload {
                                    action_type: GameActionType::Win,
                                    content: "You guessed the right word! Congratulations!!!"
                                        .to_string(),
                                };
                                let win_msg = Message::new(
                                    MessageType::GameAction,
                                    &game_action_payload.serialize(),
                                );
                                // Inform guesser that he won
                                write_all_to_stream(stream, &serialize_message(&win_msg));

                                //Inform Setter that word has been guessed
                                let c_lock = lock_client_mutex(&server.clients);

                                if let Some(opponent_stream) = c_lock.get(&opponent_id) {
                                    let game_action_payload = GameActionPayload {
                                        action_type: GameActionType::Win,
                                        content:
                                            "The opponent guessed the right word. Game is over."
                                                .to_string(),
                                    };
                                    let win_msg = Message::new(
                                        MessageType::GameAction,
                                        &game_action_payload.serialize(),
                                    );
                                    opponent_stream.send(win_msg).map_err(|send_error| {
                                        format!(
                                            "Failed to send message to opponent id {}: {:?}",
                                            opponent_id, send_error
                                        )
                                    })?;
                                }

                                println!("Player ID {} won the game", client_id);
                                game.win();
                            } else {
                                let game_state_payload = GameStatePayload {
                                    current_guesses: game.get_guesses(),
                                    hint: "".to_string(),
                                };
                                let game_state = Message::new(
                                    MessageType::GameState,
                                    &game_state_payload.serialize(),
                                );

                                let c_lock = lock_client_mutex(&server.clients);

                                if let Some(opponent_stream) = c_lock.get(&opponent_id) {
                                    opponent_stream.send(game_state).map_err(|send_error| {
                                        format!(
                                            "Failed to send message to opponent id {}: {:?}",
                                            opponent_id, send_error
                                        )
                                    })?;
                                }
                            }
                        }
                    }
                    GameActionType::Hint => {
                        let mut g_lock = lock_game_mutex(&server.games);

                        if let Some(game) = g_lock.get_mut(&game_id) {
                            // Get opponent_id and stream
                            if game.get_opponent_id(client_id).is_none() {
                                return Err("Unexpected error, player has no opponent".to_string());
                            }
                            let opponent_id = game
                                .get_opponent_id(client_id)
                                .expect("This error should not happened");

                            let game_state_payload = GameStatePayload {
                                current_guesses: 0,
                                hint: game_action_payload.content,
                            };
                            let game_state = Message::new(
                                MessageType::GameState,
                                &game_state_payload.serialize(),
                            );
                            game.add_hint(game_state_payload.hint);

                            let c_lock = lock_client_mutex(&server.clients);

                            if let Some(opponent_stream) = c_lock.get(&opponent_id) {
                                opponent_stream.send(game_state).map_err(|send_error| {
                                    format!(
                                        "Failed to send message to opponent id {}: {:?}",
                                        opponent_id, send_error
                                    )
                                })?;
                            }
                        }
                    }
                    GameActionType::Surrender => {
                        let mut g_lock = lock_game_mutex(&server.games);

                        if let Some(game) = g_lock.get_mut(&game_id) {
                            // Get opponent_id and stream
                            if game.get_opponent_id(client_id).is_none() {
                                return Err("Unexpected error, player has no opponent".to_string());
                            }
                            let opponent_id = game
                                .get_opponent_id(client_id)
                                .expect("This error should not happened");

                            let game_action_payload = GameActionPayload {
                                action_type: GameActionType::Surrender,
                                content: "You gived up. Game is over.".to_string(),
                            };
                            let surrender_msg = Message::new(
                                MessageType::GameAction,
                                &game_action_payload.serialize(),
                            );
                            // Inform guesser that he surrended
                            write_all_to_stream(stream, &serialize_message(&surrender_msg));

                            //Inform Setter that word has been guessed
                            let c_lock = lock_client_mutex(&server.clients);

                            if let Some(opponent_stream) = c_lock.get(&opponent_id) {
                                let game_action_payload = GameActionPayload {
                                    action_type: GameActionType::Surrender,
                                    content: "The opponent give up. Game is over.".to_string(),
                                };
                                let surrender_msg = Message::new(
                                    MessageType::GameAction,
                                    &game_action_payload.serialize(),
                                );
                                opponent_stream.send(surrender_msg).map_err(|send_error| {
                                    format!(
                                        "Failed to send message to opponent id {}: {:?}",
                                        opponent_id, send_error
                                    )
                                })?;
                            }

                            println!("Player ID {} give up the game", client_id);
                            game.surrender();
                        }
                    }
                    _ => {}
                }
            }
        }
        _ => {
            // Handle other types or errors
        }
    }
    Ok(())
}

// if file exist it's dropped
fn cleanup_unix_socket() {
    let _ = fs::remove_file(UNIX_SOCK_PATH);
}

// generate client_id after successful authentification
use std::sync::atomic::{AtomicU32, Ordering};
fn get_id() -> u32 {
    static COUNTER: AtomicU32 = AtomicU32::new(1);
    COUNTER.fetch_add(1, Ordering::Relaxed)
}

fn get_game_id() -> u32 {
    static COUNTER: AtomicU32 = AtomicU32::new(1);
    COUNTER.fetch_add(1, Ordering::Relaxed)
}

fn lock_client_mutex(
    clients: &Mutex<HashMap<u32, Sender<Message>>>,
) -> MutexGuard<'_, HashMap<u32, Sender<Message>>> {
    match clients.lock() {
        Ok(lock) => lock,
        Err(poisoned) => {
            // Here it depends if gracefully shut the server down or try to rely on underlying data
            // I've chosen second approach for this testing application
            eprintln!("Mutex is poisoned. Recovering...");
            poisoned.into_inner()
        }
    }
}

fn lock_game_mutex(
    games: &Mutex<HashMap<u32, GameState>>,
) -> MutexGuard<'_, HashMap<u32, GameState>> {
    match games.lock() {
        Ok(lock) => lock,
        Err(poisoned) => {
            // Here it depends if gracefully shut the server down or try to rely on underlying data
            // I've chosen second approach for this testing application
            eprintln!("Mutex is poisoned. Recovering...");
            poisoned.into_inner()
        }
    }
}

fn write_all_to_stream<S: Read + Write>(mut stream: S, ser_msg: &[u8]) {
    match stream.write_all(ser_msg) {
        Ok(()) => {}
        Err(e) => {
            eprintln!("[write_all] Failed to send message: {}", e);
        }
    }
    match stream.flush() {
        Ok(()) => {}
        Err(e) => {
            eprintln!("[flush] Failed to send message: {}", e);
        }
    }
}
