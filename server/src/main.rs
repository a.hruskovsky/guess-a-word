use std::{env, sync::Arc};

use server::ServerContext;

mod game_state;
mod server;
mod traits;

const DEFAULT_PORT: u16 = 9999;

struct Config {
    help: bool,
    port: u16,
}

fn print_help() {
    println!("\nServer for playing game \"Guess a word\"");
    println!("The default configuration of server:");
    println!("  listen on TCP 127.0.0.1:9999 and Unix socket");
    println!("  listen on TCP 127.0.0.1:8080 for HTTP requests");
    println!("\nArguments:");
    println!("  -h, --help                 Print this help message and exit.");
    println!("  -p <port>, --port <port>   Specify port number. [Default port number is 9999]");
}

fn parse_args() -> Config {
    let args: Vec<String> = env::args().collect();
    let mut config = Config {
        help: false,
        port: DEFAULT_PORT,
    };

    let mut iter = args.iter().skip(1); // Skip the name of the program
    while let Some(arg) = iter.next() {
        match arg.as_str() {
            "-h" | "--help" => {
                config.help = true;
            }
            "-p" | "--port" => {
                if let Some(port) = iter.next() {
                    let port = match port.parse::<u16>() {
                        Ok(port) => port,
                        Err(_) => {
                            eprintln!("Port number must be valid unsigned 16-bit integer");
                            std::process::exit(1);
                        }
                    };
                    config.port = port;
                }
            }
            _ => {
                eprintln!("Unknown option: {}", arg);
                std::process::exit(1);
            }
        }
    }

    config
}

fn main() -> std::io::Result<()> {
    let config = parse_args();

    if config.port == 8080 {
        println!("Port 8080 is reserved for HTTP. Pick another one");
        return Ok(());
    }

    if config.help {
        print_help();
        return Ok(());
    }

    let server = Arc::new(ServerContext::new());
    let tcp_server_handle = server::start_tcp_listener_thread(server.clone(), config.port)
        .expect("Failed to bind to port");

    let us_server_handle =
        server::start_unix_listener_thread(server.clone()).expect("Failed to bind to port");

    let html_handle = server::start_http_listener_thread(server).expect("Failed to bind to port");

    tcp_server_handle
        .join()
        .expect("Expected listener to stay alive");

    us_server_handle
        .join()
        .expect("Expected listener to stay alive");

    html_handle.join().expect("Expected listener to stay alive");

    Ok(())
}
