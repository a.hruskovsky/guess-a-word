# Guess a word

## Assignment
Well it's that easy!
---

PROLOG:

The people we are looking for should always strive to understand why things work, why things happen.
They must posses (or strive to acquire) essential knowledge about the technologies they work with.
When that's the case - its visible in the work they do.

TEST TASK:

Using a language of choice from the following:

- Rust
- Typescript
- Python
- C/C++

Without using external libraries (unless neccessary), write two applications. These applications will be a client and a server app.
They will communicate over a TCP socket and the exact "protocol" on top of that is up to you.
Note: using just utf8 strings will have a negative impact on the judgement (hint - custom binary protocol is expected).

Upon connection - the server must send a message to the client - initiating the communication.
Client upon receiving it - answers to the server with a password.
This initial exchange then ends with server either disconnecting the client (wrong password) or assigning the client an ID and sending the ID back to the client.

At this moment, the server answers to any requests the client sends to the server. For unknown requests, the server must respond as well, such that client can identify it as an error.

The main function of the server at this moment - is to facilitate game of "Guess a word" between two clients.
The game flow is as follows:

1. Client A requests a list of possible opponents (IDs)
2. Server responds with a list of possible opponents (IDs)
3. Client A requests a match with opponent (ID), specifying a word to guess
4. Server either confirms this or rejects with an error code
5. The target client - client B - is informed of the match, and can begin guesses
6. Client A is informed of the progress of Client B (attempts)
7. Client A can write an arbitrary text (a hint) that is sent to and displayed by Client B
8. Match ends when Client B guesses the word, or gives up

| Server specifics:
Must offer both Unix socket and a TCP port for client connection.

Optional/bonus: offer a website that displays the progress of all the matches, for a third party to observe.

| Client specifics:
Must be able to connect to either Unix socket or a TCP port.

RUNTIME:

Both the client and the server must run on Linux, specifically Ubuntu 22.04, without any containers or virtualization. It will be tested on x86 64bit architecture system.

JUDGEMENT:

The following things play role for passing to the interview stage:

- Understanding of both the technologies used and the language chosen.
- Complexity of the chosen solution.
- Efficiency of the custom communication protocol.
- Instructions to run the test task provided -> we will evaluate it on freshly installed Ubuntu 22.04.


## Name
Guess a word

## Description
Two applications (server and client) are implemented in this repository. When server is running clients may connect to it and play a Guess a word game against each other. Both application are written in Rust language (https://www.rust-lang.org/). There is also optional extension application written for this game. The website that displays the progress of all the matches. Application is written in two versions. First is written in React and can be found here https://gitlab.com/a.hruskovsky/guess-a-word-overview. Second version is in plain HTML + CSS + JS and can be found here https://gitlab.com/a.hruskovsky/guess-a-word-overview-plain.

### Server
Server support both TCP and Unix socket connections simultaneously. Server is running on localhost on default port 9999. Custom port can be configured in parameter. Server has a simple authentication implemented, so if client try to connect with wrong password the connection is closed. Server also support simple HTTP connection for application "Game overview" (https://gitlab.com/a.hruskovsky/guess-a-word-overview-plain). It's listening on port 8080.

*Hint: The correct password to successfully connect to the server is:* **password**

Running the server application
	```cargo run```

### Client
Architecture of the client application is simple state machine. Client can communicate via TCP or Unix socket. The communication type is entered in the parameter. 

Connecting to server via TCP socket
	```cargo run -- -i 127.0.0.1 -p 9999 --pwd password```

Connecting to server via Unix socket
	```cargo run -- --unix --pwd password```

The functionality of client depends on "game mode" in which client is. First is before he start the game "Commend mode". In this mode client should pick an opponent to play. To do this, he has two commands:
- "list" - list all possible opponents (list of opponent IDs)
- "play **x word**" - choose an opponent "x" to play a game with and the word to guess is "word"

Game can be played in One-on-One mode only. Client is then in one of this "Game modes": 
- Setter
- Guesser

#### Setter
In this mode client see the progress of guesses (number of attempts) and can send a hints to his opponent (guesser). He is informed when the opponent surrenders or guesses the correct word. When game is over client return back to "Command mode" and he can initiate other game or become a new guesser if someone challenges him.

#### Guesser
In this mode client is informed that he's been challenged by different player and he try to guess the correct word. Guesser can receive hints from the one who challended him (Setter). If he guess the correct word, he's informed that he won. If guesser has no clue, he also may give up. In this case game is also over and client return back to "Command mode". He can initiate other game or become a guesser again if someone challenges him. To be able to submit the guesses or surrender, the commands bellow must be used:
- "g **word**" – by this command player is telling to server he wants to guess a "word" (first letter is command and "g“ stands for guess)
- "s" – this command means that player want to surrender ("s“ stands for surrender)


### Guess a word overview application
Also third application as extension has been made (website that displays the progress of all the matches). Two versions of this extension application has been made.

#### Plain HTML + CSS + JS application overview
Implementation is written in plain HTML + CSS + JS and can be found here https://gitlab.com/a.hruskovsky/guess-a-word-overview

#### React application overview
Implementation is written in Rust and can be found here https://gitlab.com/a.hruskovsky/guess-a-word-overview. 

## Installation
Applications are written in Rust language, so Rustup needs to be installed.

1. Visit https://www.rust-lang.org/tools/install and install rust on your computer
2. Download or clone this project (git clone https://gitlab.com/a.hruskovsky/guess-a-word.git)
3. Inside folder *guess-a-word* run  ```cargo build```
4. run server
5. connect with clients

## Usage
Server application listen on both TCP and Unix sockets. I also implemented a really simple HTTP server listening on IP 127.0.0.1 and port 8080. Default port where client application can connect is set to 9999. If you want to use different server just add parameter "--port" and specify your own port number. For printing a help for server application type parameter "--help". More info how to run server:

```
-- Switch to server folder
$ cd ./server
-- Run server application listening on port 9999
$ cargo run
-- If different port is required (for example 8888) run application like bellow
$ cargo run -- --port 8888
```

Client application support communication via TCP socket or UNIX socket. To run client application IP address of server and port needs to be specified in parameter. For correct authentization also password needs to be specified in parameter. The correct password is "password" (it's hardcoded for simplicity). For more information help can be printed with parameter "--help". More info how to run client:

```
-- Switch to client folder
$ cd ./client
-- Run client application communicating via TCP
$ cargo run -- --ip 127.0.0.1 --port 9999 --pwd password
-- Run client application communicating via Unix socket
$ cargo run -- --unix --pwd password
```

### Commands for client
1. When client is connected and authenticated to server it's in command mode. The commands are:

   "**list**" - retrieve a list of players who are connected to server and available to play (not in any active game)

   "**play X WORD**" - command for start a game with opponent_id "X" and "WORD" which opponent need to guess (so for example the command may look like "play 1 apple")
2. When game is started the challenger may send a hint (just a plain text) to opponent
3. The opponent have two possibilities what to do (guess or give up).
   For guessing a word, opponent need to write letter 'g' followed by word:

   g banana

   For give up the game opponent needs to write letter 's' (only letter s)

   s

Those are all possible options how to communicate and play the game

## Project status
Done
