use common::{
    deserialize_message, hash, serialize_message, AuthPayload, AuthResponsePayload,
    ClientsListPayload, CommunicationType, ErrorCode, ErrorPayload, GameActionPayload,
    GameActionType, GameRequestPayload, GameStatePayload, Message, MessageType, BUFFER_LEN,
    UNIX_SOCK_PATH,
};
use std::{
    env,
    io::{self, Read, Write},
    net::TcpStream,
    os::unix::net::UnixStream,
    sync::{
        atomic::{AtomicBool, Ordering},
        mpsc::{self, Receiver, Sender},
        Arc,
    },
    thread,
    time::Duration,
};
use utils::is_valid_ip;

use crate::utils::sanitize_input;

mod utils;

struct Config {
    help: bool,
    ip: String,
    port: Option<u16>,
    com_type: CommunicationType,
    pwd: String,
}

fn print_help() {
    println!("\nClient for playing game \"Guess a word\"");
    println!("Client is able to communicate over Unix socket (argument --unix)");
    println!("or via TCP socket which is default way of communication.");
    println!("\nArguments:");
    println!("  -h, --help                     Print this help message and exit.");
    println!("  --unix                         Use UNIX socket communication.");
    println!("  -i <ip_addr>, --ip <ip_addr>   Specify port number. [Default port number is 9999]");
    println!("  -p <port>, --port <port>       Specify port number. [Default port number is 9999]");
    println!("  --pwd <password>               Specify password for connecting to server- [Password is \"password\"]");
}

fn parse_args() -> Config {
    let args: Vec<String> = env::args().collect();
    let mut config = Config {
        help: false,
        ip: String::from(""),
        port: None,
        com_type: CommunicationType::Tcp,
        pwd: String::from(""),
    };

    let mut iter = args.iter().skip(1); // Skip the name of the program
    while let Some(arg) = iter.next() {
        match arg.as_str() {
            "-h" | "--help" => {
                config.help = true;
            }
            "--unix" => {
                if !config.ip.is_empty() {
                    eprintln!("Conflicting communication options provided. Choose between TCP or Unix sockets. Not both together");
                    std::process::exit(1);
                }
                config.com_type = CommunicationType::UnixSocket;
            }
            "-i" | "--ip" => {
                if config.com_type == CommunicationType::UnixSocket {
                    eprintln!("Conflicting communication options provided. Choose between TCP or Unix sockets. Not both together");
                    std::process::exit(1);
                }

                if let Some(ip_addr) = iter.next() {
                    if is_valid_ip(ip_addr) {
                        config.ip = ip_addr.to_owned();
                    } else {
                        eprintln!("Invalid IP address {}", ip_addr);
                        std::process::exit(1);
                    }
                } else {
                    eprintln!("Missing IP address");
                    std::process::exit(1);
                }
            }
            "-p" | "--port" => {
                if config.com_type == CommunicationType::UnixSocket {
                    eprintln!("Conflicting communication options provided. Choose between TCP or Unix sockets. Not both together");
                    std::process::exit(1);
                }

                if let Some(port) = iter.next() {
                    config.port = match port.parse::<u16>() {
                        Ok(port) => Some(port),
                        Err(_) => {
                            eprintln!("Port number must be valid unsigned 16-bit integer");
                            std::process::exit(1);
                        }
                    };
                }
            }
            "--pwd" => {
                if let Some(pwd) = iter.next() {
                    config.pwd = pwd.trim().to_owned();
                }
            }
            _ => {
                eprintln!("Unknown option: {}", arg);
                std::process::exit(1);
            }
        }
    }

    config
}

#[derive(PartialEq)]
enum ClientState {
    Unauthenticated,
    Auhenticating,
    Authenticated,
    Playing,
    Guessing,
}

struct Client {
    id: u32,
    state: ClientState,
}

fn main() -> std::io::Result<()> {
    let application_stop = Arc::new(AtomicBool::new(false));
    let config = parse_args();
    let mut client = Client {
        id: 0,
        state: ClientState::Unauthenticated,
    };

    let (tx, rx) = mpsc::channel::<Message>();

    if config.help {
        print_help();
        return Ok(());
    }

    match config.com_type {
        CommunicationType::Tcp => {
            if config.ip.is_empty() || config.port.is_none() {
                eprintln!(
                    "One or both options are missing for TCP connection: IP: \"{}\", port: {:?}",
                    config.ip, config.port
                );
                std::process::exit(1);
            }

            let stream = TcpStream::connect(format!(
                "{}:{}",
                config.ip,
                config
                    .port
                    .expect("Port number was not set for TCP connection")
            ))
            .expect("Failed to connect to server");
            stream.set_nonblocking(true)?;

            let stream_clone = stream.try_clone()?;

            handle_stream(stream, tx, application_stop.clone());
            state_machine(
                &mut client,
                &config,
                rx,
                stream_clone,
                application_stop.clone(),
            )?;
        }
        CommunicationType::UnixSocket => {
            let stream = UnixStream::connect(UNIX_SOCK_PATH).expect("Failed to connect to server");
            stream.set_nonblocking(true)?;

            let stream_clone = stream.try_clone()?;

            handle_stream(stream, tx, application_stop.clone());
            state_machine(
                &mut client,
                &config,
                rx,
                stream_clone,
                application_stop.clone(),
            )?;
        }
    };

    Ok(())
}

fn handle_stream<S: Read + Write + Send + 'static>(
    mut stream: S,
    tx: Sender<Message>,
    application_stop: Arc<AtomicBool>,
) {
    thread::spawn(move || {
        loop {
            let mut buffer = [0u8; BUFFER_LEN];
            match stream.read(&mut buffer) {
                Ok(0) => {
                    println!("Client disconnected");
                    tx.send(Message::new(MessageType::Disconnect, &[]))
                        .unwrap_or(());
                    return;
                }
                Ok(_size) => {
                    // Check if message from server is valid, otherwise ignore it
                    if let Ok(message) = deserialize_message(&buffer) {
                        if tx.send(message).is_err() {
                            application_stop.store(true, Ordering::SeqCst);
                            return;
                        }
                    }
                }
                Err(ref e) if e.kind() == io::ErrorKind::WouldBlock => {
                    // No data available yet
                    continue; // Optionally sleep or yield
                }
                Err(e) => {
                    eprintln!("Error: {}", e);
                    break;
                }
            }
            std::thread::sleep(Duration::from_millis(10));
        }
    });
}

fn state_machine<S: Read + Write>(
    client: &mut Client,
    config: &Config,
    rx: Receiver<Message>,
    mut stream: S,
    application_stop: Arc<AtomicBool>,
) -> std::io::Result<()> {
    let application_stop_clone = application_stop.clone();
    let (input_tx, input_rx) = mpsc::channel::<String>();
    // Thread for handling stdin input
    let input_thread = thread::spawn(move || {
        let mut input = String::new();
        while io::stdin().read_line(&mut input).is_ok()
            && !application_stop_clone.load(Ordering::SeqCst)
        {
            if input_tx.send(input.clone()).is_err() {
                application_stop_clone.store(true, Ordering::SeqCst);
                return;
            }
            input.clear();
        }
    });

    loop {
        // Handle user input from separate thread
        if let Ok(input) = input_rx.try_recv() {
            // Process the command from the user
            match client.state {
                ClientState::Authenticated => process_user_command(&input, &mut stream)?,
                ClientState::Playing | ClientState::Guessing => {
                    process_user_input(&input, &mut stream, &client.state)?
                }
                _ => {}
            }
        }

        if let Ok(message) = rx.recv_timeout(Duration::from_millis(300)) {
            if message.header.message_type == MessageType::Disconnect {
                println!("Disconnected from server!");
                return Ok(());
            }

            match client.state {
                ClientState::Unauthenticated => {
                    if message.header.message_type == MessageType::Initiate {
                        // Send an authenticate message
                        let auth_payload = AuthPayload {
                            password: hash(&config.pwd).to_string(),
                        };

                        let auth_message = Message::new(
                            MessageType::Authenticate,
                            auth_payload.password.as_bytes(),
                        );
                        stream.write_all(&serialize_message(&auth_message))?;
                        stream.flush()?;
                        client.state = ClientState::Auhenticating;
                    }
                }
                ClientState::Auhenticating => {
                    if message.header.message_type == MessageType::AuthenticateResponse {
                        let payload = AuthResponsePayload::deserialize(&message.payload);
                        match payload {
                            Some(p) => {
                                if !p.status {
                                    application_stop.store(true, Ordering::SeqCst);
                                    break;
                                }

                                if p.client_id.is_none() {
                                    application_stop.store(true, Ordering::SeqCst);
                                    break;
                                }

                                client.id = p.client_id.expect("Unexpected error, because client_id can not be None in this line");
                                client.state = ClientState::Authenticated;
                                println!("Enter commands: ");
                            }
                            None => {
                                application_stop.store(true, Ordering::SeqCst);
                                break;
                            }
                        }
                    }
                }
                ClientState::Authenticated => match message.header.message_type {
                    MessageType::ListClientsResponse => {
                        if let Some(list) = ClientsListPayload::deserialize(&message.payload) {
                            let clients_str: Vec<String> =
                                list.client_ids.iter().map(|&num| num.to_string()).collect();
                            println!("\nList of players:\n{}", clients_str.join("\n"));
                        }
                    }
                    MessageType::GameRequestResponse => {
                        if let Some(response) = ErrorPayload::deserialize(&message.payload) {
                            match response.code {
                                ErrorCode::NoError => {
                                    client.state = ClientState::Playing;
                                    println!("You may enter hints: ");
                                }
                                _ => {
                                    println!("{}", response.text);
                                }
                            }
                        }
                    }
                    MessageType::GameAction => {
                        if let Some(action) = GameActionPayload::deserialize(&message.payload) {
                            if action.action_type == GameActionType::GuessWord {
                                println!("{}", &action.content);
                                client.state = ClientState::Guessing;
                            }
                        }
                    }
                    _ => {}
                },
                ClientState::Playing => match message.header.message_type {
                    MessageType::GameAction => {
                        if let Some(action) = GameActionPayload::deserialize(&message.payload) {
                            match action.action_type {
                                GameActionType::Win => {
                                    println!("{}", &action.content);
                                    client.state = ClientState::Authenticated;
                                    println!("Enter commands: ");
                                }
                                GameActionType::Guess => {
                                    println!("{}", &action.content);
                                }
                                GameActionType::Surrender => {
                                    println!("Opponent give up. Game is over");
                                    client.state = ClientState::Authenticated;
                                    println!("Enter commands: ");
                                }
                                _ => {}
                            }
                        }
                    }
                    MessageType::GameState => {
                        if let Some(state) = GameStatePayload::deserialize(&message.payload) {
                            println!("Number of Attempts: {}", state.current_guesses);
                        }
                    }
                    _ => {}
                },
                ClientState::Guessing => match message.header.message_type {
                    MessageType::GameAction => {
                        if let Some(action) = GameActionPayload::deserialize(&message.payload) {
                            match action.action_type {
                                GameActionType::Win => {
                                    println!("{}", &action.content);
                                    client.state = ClientState::Authenticated;
                                    println!("Enter commands: ");
                                }
                                GameActionType::Surrender => {
                                    println!("You gived up. Game is over");
                                    client.state = ClientState::Authenticated;
                                    println!("Enter commands: ");
                                }
                                _ => {}
                            }
                        }
                    }
                    MessageType::GameState => {
                        if let Some(state) = GameStatePayload::deserialize(&message.payload) {
                            println!("New hint for you is: \"{}\"", state.hint);
                        }
                    }
                    _ => {}
                },
            }
        }
    }
    input_thread
        .join()
        .expect("The thread being joined has panicked");
    Ok(())
}

fn process_user_command<S: Write>(input: &str, stream: &mut S) -> std::io::Result<()> {
    let command = sanitize_input(input);
    let parts: Vec<&str> = command.split(' ').collect();
    match parts[0] {
        "list" if parts.len() == 1 => {
            let message = Message::new(MessageType::ListClients, "list".as_bytes());
            stream.write_all(&serialize_message(&message))?;
            stream.flush()?;
        }
        "play" if parts.len() == 3 => match parts[1].parse::<u32>() {
            Ok(opponent_id) if !parts[2].is_empty() => {
                let game_request_payload = GameRequestPayload {
                    opponent_id,
                    word_to_guess: parts[2].to_string(),
                };
                let message =
                    Message::new(MessageType::GameRequest, &game_request_payload.serialize());
                stream.write_all(&serialize_message(&message))?;
                stream.flush()?;
            }
            Ok(_) => {
                println!("Error: The word to guess cannot be empty.");
            }
            Err(_) => {
                println!("Error: Opponent ID must be a valid unsigned 32-bit integer.");
            }
        },
        _ => {
            println!("Unknown command: {}", command);
        }
    }
    Ok(())
}

fn process_user_input<S: Write>(
    input: &str,
    stream: &mut S,
    state: &ClientState,
) -> std::io::Result<()> {
    let user_input = sanitize_input(input);

    match state {
        ClientState::Playing => {
            let game_action_payload = GameActionPayload {
                action_type: GameActionType::Hint,
                content: user_input.to_string(),
            };
            let message = Message::new(MessageType::GameAction, &game_action_payload.serialize());
            stream.write_all(&serialize_message(&message))?;
            stream.flush()?;
        }
        ClientState::Guessing => {
            let parts: Vec<&str> = user_input.split(' ').collect();
            match parts[0] {
                "g" if parts.len() == 2 => {
                    let game_action_payload = GameActionPayload {
                        action_type: GameActionType::Guess,
                        content: parts[1].to_string(),
                    };
                    let message =
                        Message::new(MessageType::GameAction, &game_action_payload.serialize());
                    stream.write_all(&serialize_message(&message))?;
                    stream.flush()?;
                }
                "s" if parts.len() == 1 => {
                    let game_action_payload = GameActionPayload {
                        action_type: GameActionType::Surrender,
                        content: "".to_string(),
                    };
                    let message =
                        Message::new(MessageType::GameAction, &game_action_payload.serialize());
                    stream.write_all(&serialize_message(&message))?;
                    stream.flush()?;
                }
                _ => {
                    println!("Invalid user input");
                }
            }
        }
        _ => {
            println!("[process_user_input] We should never get into this");
        }
    }

    Ok(())
}
