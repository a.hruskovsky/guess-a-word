use common::MAX_USER_INPUT_LEN;

const MAX_IP_ADDR_LEN: usize = 15;

pub fn is_valid_ip(ip: &str) -> bool {
    // sanitize input (without external crates only trim)
    let ip = ip.trim();

    if ip.len() > MAX_IP_ADDR_LEN {
        return false;
    }

    let parts: Vec<&str> = ip.split('.').collect();

    if parts.len() != 4 {
        return false;
    }

    for part in parts {
        // Check if the part is a valid number between 0 and 255
        if let Ok(_num) = part.parse::<u8>() {
            // Check for leading zeros
            if part != "0" && part.starts_with('0') {
                return false;
            }
        } else {
            return false;
        }
    }

    true
}

pub fn sanitize_input(input: &str) -> String {
    // Trim whitespace and limit length to 50 characters
    let trimmed = input.trim();
    let lower_case = trimmed.to_ascii_lowercase();
    let max_length = MAX_USER_INPUT_LEN;
    if lower_case.len() > max_length {
        println!(
            "Maximum size of user input is {}.\nYour string was limited to: \"{}\"",
            MAX_USER_INPUT_LEN,
            &lower_case[..max_length].to_string()
        );
        lower_case[..max_length].to_string()
    } else {
        lower_case.to_string()
    }
}
